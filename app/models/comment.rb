class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :post:id
	validates_presence_of :body
end
